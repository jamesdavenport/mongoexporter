import os, sys, datetime,time, json, subprocess, re
from datetime import date

def exportRecordsToJson(database, collection, timeStamp):
    #currentTimeMilliseconds = int(round(time.time()*1000 ))
    #oneHundredEightyDays = 15552000000
    oneDayLimit = timeStamp + 86400000
    
    os.chdir('/home/ec2-user/ArchivePythonCode')
    today = date.today().strftime("%m.%d.$y")
    archiveName = str(oneDayLimit) + today + '-' + db + '-' + collection + '.json'
    os.system('/usr/bin/./mongoexport --db %s --collection %s --query  \'{"timeStamp":{"$gte":%s, "$lte":%s}}\'    --out %s'   % (db, collection, timeStamp, oneDayLimit , archiveName))

def timeStampParser(mongoRecord):
    recordSplit = mongoRecord.split("NumberLong")
    return  exportLoop(int(re.search(r'\d+', recordSplit[1]).group()))

def exportLoop(timeStampInSeconds):
   listOfTimeStamps = [] 

   while(timeStampInSeconds  < time.time()*1000):
    listOfTimeStamps.append(timeStampInSeconds)
    timeStampInSeconds += 86400000

   for stamp in listOfTimeStamps:
    exportRecordsToJson(db , collection , stamp)

def mongoShellUtil(database, query):
    os.system('/usr/bin/./mongo %s --eval  %s' % (database, query))

def findOldestRecord(database, collection):
    query =  "db" + "." + collection + "." + "find({}).sort({\"timeStamp\": 1}).limit(1)"
    os.chdir('/usr/bin')
    result = subprocess.Popen([ 'mongo',  database ,  '--eval' , query  ],stdout=subprocess.PIPE)
    timeStampParser(result.communicate()[0])

def findLatestRecord(database, collection):
    query =  "db" + "." + collection + "." + "find({}).sort({\"timeStamp\": -1}).limit(1)"
    os.chdir('/usr/bin')
    result = subprocess.Popen([ 'mongo',  database ,  '--eval' , query  ],stdout=subprocess.PIPE)
    oldestTimeStamp = timeStampParser(result.communicate()[0])

def countRecords(database, collection):
    query = '"' + "db" + "." + collection + "." + "count()" + '"'
    mongoShellUtil(database , query)

def deleteRecords(database , collection):
    query = '"' + "db" + "." + collection + "." + "remove({})" + '"'
    mongoShellUtil(database , query)

if __name__ =="__main__":
    db = sys.argv[1]
    collection = sys.argv[2]
    #exportRecordsToJson(db,  collection)
    #countRecords(db, collection)
    findOldestRecord(db, collection)
    #findLatestRecord(db, collection)
    #exportLoop()
